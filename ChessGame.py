import random
from enum import Enum
from ChessPieces import Rook, Knight, King, Queen, Bishop, Pawn
from ChessExceptions import NotYourTurnException, CannnotTakeOwnPieceException, PathObstructedException, \
                            InvalidMoveException, OutOfBoundsException

class ChessPlayer(Enum):
    BLACK = "black"
    WHITE = "white"

class ChessGame:

    def __init__(self, blank = False, print_board = False):
        self.taken_pieces = None
        self.turn = None
        self.board = self.blank_board() if blank else self.setup_board()
        self.start_game()
        if print_board:
            self._print_board()
            print(self.turn + " starts this game.")

    def start_game(self):
        self.turn = random.choice([ChessPlayer.WHITE,ChessPlayer.BLACK])
        self.taken_pieces = []

    def _is_valid_move(self, start, end):
        within_bounds = self._is_within_bounds(start) and self._is_within_bounds(end)
        return within_bounds

    def _are_within_bounds(self, *args):
        return all([self._is_within_bounds(arg) for arg in args])

    def _is_within_bounds(self, coord):
        x, y = coord
        return x >= 0 and x < 8 and y >= 0 and y < 8

    def make_move(self, start, end, print_board = False):
        start_x, start_y, end_x, end_y, start_piece, end_piece = self._expand(start, end)
        if start_piece:
            if start_piece.player_owner != self.turn:
                raise NotYourTurnException(start_piece, self.turn)
            if self._are_within_bounds(start,end):
                potential_moves = start_piece.potential_moves(start)
                if end in potential_moves:
                    path_blocked = self.is_path_blocked(start, end)
                    if path_blocked:
                        raise PathObstructedException(path_blocked)
                    else:
                        self.move_piece(start, end)
                else:
                    raise InvalidMoveException(start_piece)
            else:
                raise OutOfBoundsException(start, end)
        if print_board:
            self._print_board()

    def move_piece(self, start, end):
        start_x, start_y, end_x, end_y, start_piece, end_piece = self._expand(start, end)
        if end_piece:
            self.take_piece(start_piece, end_piece)
        self.board[start_x][start_y] = None
        self.board[end_x][end_y] = start_piece
        self.turn = ChessPlayer.WHITE if self.turn == ChessPlayer.BLACK else ChessPlayer.BLACK

    def take_piece(self, start_piece, end_piece):
        if end_piece.player_owner != start_piece.player_owner:
            self.taken_pieces.append(end_piece)
        else:
            raise CannnotTakeOwnPieceException(end_piece)

    def is_path_blocked(self, start, end):
        start_x, start_y, end_x, end_y, start_piece, end_piece = self._expand(start, end)
        path = start_piece.get_path(start, end)
        for x, y in path:
            piece = self.board[x][y]
            if piece and piece != start_piece and piece != end_piece:
                return piece

    def _expand(self, start, end):
        start_x, start_y = start
        end_x, end_y = end
        # Unhandled index error here if start and end are out of bounds.
        start_piece = self.board[start_x][start_y]
        end_piece = self.board[end_x][end_y]
        return start_x, start_y, end_x, end_y, start_piece, end_piece

    def blank_board(self):
        board = []
        for row in range(8):
            board.append([None for col in range(8)])
        return board

    def setup_board(self):
        board = self.blank_board()
        p1 = ChessPlayer.WHITE
        p2 = ChessPlayer.BLACK

        board[0][0] = Rook(p1)
        board[7][0] = Rook(p1)
        board[0][7] = Rook(p2)
        board[7][7] = Rook(p2)

        board[1][0] = Knight(p1)
        board[6][0] = Knight(p1)
        board[1][7] = Knight(p2)
        board[6][7] = Knight(p2)

        board[2][0] = Bishop(p1)
        board[5][0] = Bishop(p1)
        board[2][7] = Bishop(p2)
        board[5][7] = Bishop(p2)

        board[4][0] = King(p1)
        board[3][0] = Queen(p1)
        board[3][7] = King(p2)
        board[4][7] = Queen(p2)

        for x in range(8):
            board[x][1] = Pawn(p1)
            board[x][6] = Pawn(p2)

        return board

    def _print_board(self):
        result = zip(*self.board)
        for row in result:
            for cell in row:
                print('-'.ljust(9) if not cell else str(cell).ljust(9),end=' ')
            print("")
        print("Taken: " + str(self.taken_pieces))