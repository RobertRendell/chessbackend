from ChessGame import ChessPlayer


class ChessPiece(object):

    def __init__(self, player_owner=ChessPlayer.WHITE):
        self.player_owner = player_owner

    def potential_moves(self, position):
        # Returns a set of coordinates a piece could move to given a position.
        # Each implementation will be different depending on the movement behaviour of the piece.
        raise NotImplementedError()

    def get_path(self, start, end):
        # Returns a list of coordinates along the start to end path.
        raise NotImplementedError()

    def neighbours(self, coord, depth=1, include_diagonals=True):
        x, y = coord
        diagonals = set([(x + depth, y - depth),
                         (x - depth, y - depth),
                         (x + depth, y + depth),
                         (x - depth, y + depth),])
        cross = set([(x, y + depth),
                    (x, y - depth),
                    (x + depth, y),
                    (x - depth, y),])
        return cross.union(diagonals) if include_diagonals else cross

    def __str__(self):
        return str(self.player_owner[0]) + "."


class Rook(ChessPiece):
    def potential_moves(self, position):
        # Returns a set of potential moves for a Rook.
        # Chess pieces on their own don't have any knowledge about other pieces.
        moves = set([])
        cx, cy = position
        xs = set([(x, cy) for x in range(8) if x != cx])
        ys = set([(cx, y) for y in range(8) if y != cy])
        moves = moves.union(xs).union(ys)
        return moves

    def get_path(self, start, end):
        # Assume the end point is in the set of potential moves.
        ax, ay = start
        bx, by = end
        dx = bx - ax
        dy = by - ay
        # Rooks can't move along both axis in the same move.
        if dx:
            x_dir = int(dx / abs(dx))
            return [(ax + (x_dir * x), ay) for x in range(abs(dx))]
        if dy:
            y_dir = int(dy / abs(dy))
            return [(ax, ay + (y_dir * y)) for y in range(abs(dy))]
        return [end] #  if there is no difference between start and end

    def __str__(self):
        return super().__str__() + self.__class__.__name__


class Bishop(ChessPiece):
    def potential_moves(self, position):
        cx, cy = position
        # Bishop must move along both axis at the same time, in either direction
        # This will return moves outside the bounds of the board.
        a = set([(cx-step, cy-step) for step in range(1,8)])
        b = set([(cx+step, cy+step) for step in range(1,8)])
        c = set([(cx-step, cy+step) for step in range(1,8)])
        d = set([(cx+step, cy-step) for step in range(1,8)])
        return a.union(b, c, d)

    def get_path(self, start, end):
        # Assume the end point is in the set of potential moves.
        ax, ay = start
        bx, by = end
        # Bishops must move along both axis at the same time, in either direction.
        # dx and dy tell us the direction we are moving in each axis.
        dx = bx - ax
        dy = by - ay

        x_dir = int(dx / abs(dx))
        y_dir = int(dy / abs(dy))
        return [(ax + (x_dir * step), ay + (y_dir * step)) for step in range(abs(dy))]

    def __str__(self):
        return super().__str__() + self.__class__.__name__


class Queen(ChessPiece):
    # A queen is a special piece because it is a combination of a rook and a bishop, compose both when we initialise it.
    # We need to compose both because potential_moves can't be made static, pawns need to access the instance.
    bishop = None
    rook = None

    def __init__(self, player_owner):
        super().__init__(player_owner)
        self.bishop = Bishop()
        self.rook = Rook()

    def potential_moves(self, position):
        # Compose a bishop and a rook, then generate their moves based on the position.
        bishop_moves = self.bishop.potential_moves(position)
        rook_moves = self.rook.potential_moves(position)
        return bishop_moves.union(rook_moves)

    def get_path(self, start, end):
        # Decide whether the queen move was a bishop type move or rook move then use their path algorithm.
        if self.is_bishop_type_move(start, end):
            return self.bishop.get_path(start, end)
        return self.rook.get_path(start, end)

    def is_bishop_type_move(self, start, end):
        ax, ay = start
        bx, by = end
        dx = bx - ax
        dy = by - ay
        return abs(dx) == abs(dy)

    def __str__(self):
        return super().__str__() + self.__class__.__name__


class King(ChessPiece):
    def potential_moves(self, position):
        x, y = position
        return self.neighbours(position)

    def get_path(self, start, end):
        return [start, end]

    def __str__(self):
        return super().__str__() + self.__class__.__name__


class Pawn(ChessPiece):
    y_direction = 1
    def __init__(self, player_owner):
        if player_owner == ChessPlayer.BLACK:
            self.y_direction = -1
        super().__init__(player_owner)

    def potential_moves(self, position):
        x, y = position
        return set([(x, y + self.y_direction),(x+1, y + self.y_direction),(x-1, y + self.y_direction)])

    def get_path(self, start, end):
        return [start, end] #  This causes an issue for is_path_blocked because path[:-1]

    def __str__(self):
        return super().__str__() + self.__class__.__name__


class Knight(ChessPiece):
    def potential_moves(self, position):
        px, py = position
        neighbours = self.neighbours(position, depth=2, include_diagonals=False)
        moves = set([])
        for nx, ny in neighbours:
            if nx < px or nx > px:
                moves = moves.union([(nx, ny + step) for step in [1, -1]])
            else:
                moves = moves.union([(nx + step, ny) for step in [1, -1]])
        return moves



    def get_path(self, start, end):
        # The path is only used to check if it is blocked, knights don't get blocked.
        return [start,end]

    def __str__(self):
        return super().__str__() + self.__class__.__name__