class NotYourTurnException(Exception):
    def __init__(self, piece, turn):
        super().__init__("Trying to move a " + piece.player_owner + " piece but it's " + turn + "'s turn.")

class CannnotTakeOwnPieceException(Exception):
    def __init__(self, piece_tried_to_take):
        super().__init__("You can't take your own piece ("+str(piece_tried_to_take)+").")

class PathObstructedException(Exception):
    def __init__(self, obstruction):
        super().__init__("The path is not clear, obstructed by " + str(obstruction))

class InvalidMoveException(Exception):
    def __init__(self, piece):
        super().__init__("That move is invalid for the piece " + str(piece))

class OutOfBoundsException(Exception):
    def __init__(self, *args):
        super().__init__("One of the following is out of bounds " + str(args))

