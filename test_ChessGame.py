import unittest
from ChessGame import ChessGame, ChessPlayer
from ChessPieces import ChessPiece, Rook, Pawn, Bishop, King, Queen, Knight
from ChessExceptions import NotYourTurnException, CannnotTakeOwnPieceException, PathObstructedException, \
                            InvalidMoveException, OutOfBoundsException


class TestChess(unittest.TestCase):

    def setUp(self):
        self.chess = ChessGame( blank = True )
        self.chess.turn = self.chess.white
        self.white_piece1 = ChessPiece('white')
        self.white_piece2 = ChessPiece('white')
        self.black_piece = ChessPiece('black')

    def test_cant_take_own_piece(self):
        self.chess.board[0][0] = self.white_piece1
        self.chess.board[0][3] = self.white_piece2
        self.white_piece1.potential_moves = lambda x: set([(0,3)])
        self.white_piece1.get_path = lambda x,y: [(0,0),(0,3)]

        self.assertRaises(CannnotTakeOwnPieceException,self.chess.make_move,(0,0),(0,3))

        self.assertEqual(self.white_piece2, self.chess.board[0][3])
        self.assertEqual(self.white_piece1, self.chess.board[0][0])

    def test_take_piece(self):
        self.chess.board[0][0] = self.white_piece1
        self.chess.board[0][3] = self.black_piece
        self.white_piece1.potential_moves = lambda x: set([(0,3)])
        self.white_piece1.get_path = lambda x,y: [(0,0),(0,3)]

        self.chess.make_move((0,0),(0,3))

        self.assertEqual(self.white_piece1, self.chess.board[0][3])
        self.assertEqual(self.chess.taken_pieces, [self.black_piece])
        self.assertEqual(None, self.chess.board[0][0])

    def test_not_your_turn(self):
        self.chess.turn = self.chess.black
        self.chess.board[0][0] = self.white_piece1

        self.assertRaises(NotYourTurnException,self.chess.make_move,(0,0),(0,3))

        self.assertEqual(self.white_piece1, self.chess.board[0][0])

    def test_move_out_of_bounds(self):
        self.chess.board[0][0] = self.white_piece1

        self.assertRaises(OutOfBoundsException,self.chess.make_move,(0, 0), (-1, 3))

        self.assertEqual(self.white_piece1, self.chess.board[0][0])

    def test_same_start_end(self):
        self.chess.board[0][0] = self.white_piece1
        self.white_piece1.potential_moves = lambda x: set([(0, 3)])
        self.white_piece1.get_path = lambda x, y: [(0, 0), (0, 3)]

        self.assertRaises(InvalidMoveException, self.chess.make_move,(0, 0), (0, 0))

        self.assertEqual(self.white_piece1, self.chess.board[0][0])


class TestRook(unittest.TestCase):

    def setUp(self):
        self.chess = ChessGame( blank = True )
        self.chess.turn = self.chess.white
        self.rook = Rook('white')

    def test_rook_move(self):
        self.chess.board[0][0] = self.rook
        self.chess.make_move((0,0),(0,5))
        self.assertEqual(self.rook, self.chess.board[0][5])  # Can rook move down?
        self.chess.turn = self.chess.white
        self.chess.make_move((0, 5), (1, 5))
        self.assertEqual(self.rook, self.chess.board[1][5])  # Can rook move right?
        self.chess.turn = self.chess.white
        self.chess.make_move((1, 5), (1, 3))
        self.assertEqual(self.rook, self.chess.board[1][3])  # Can rook move up?
        self.chess.turn = self.chess.white
        self.chess.make_move((1, 3), (0, 3))
        self.assertEqual(self.rook, self.chess.board[0][3])  # Can rook move left?

    def test_rook_blocked_by_other_piece(self):
        pawn = ChessPiece('white')
        self.chess.board[0][0] = self.rook
        self.chess.board[0][3] = pawn

        self.assertRaises(PathObstructedException,self.chess.make_move,(0,0),(0,5))

        self.assertIsNone(self.chess.board[0][5])
        self.assertEqual(self.rook, self.chess.board[0][0])

    def test_rook_invalid_move(self):
        rook = Rook('white')
        self.chess.board[0][2] = rook

        # Try to move the rook diagonally
        self.assertRaises(InvalidMoveException,self.chess.make_move,(0, 2), (1, 3))

        self.assertEqual(rook, self.chess.board[0][2])
        self.assertIsNone(self.chess.board[1][3])


class TestPawn(unittest.TestCase):

    def setUp(self):
        self.chess = ChessGame( blank = True )
        self.chess.turn = self.chess.white
        self.pawn = Pawn('white')

    def test_pawn_move(self):
        self.chess.board[0][0] = self.pawn
        self.chess.make_move((0,0),(0,1))
        self.assertEqual(self.pawn, self.chess.board[0][1])

    def test_pawn_blocked_by_other_piece(self):
        another_piece = ChessPiece('black')
        self.chess.board[0][0] = self.pawn
        self.chess.board[0][1] = another_piece

        self.assertRaises(PathObstructedException,self.chess.make_move,(0,0),(0,1))

        self.assertEqual(another_piece, self.chess.board[0][1])
        self.assertEqual(self.pawn, self.chess.board[0][0])

    def test_pawn_invalid_move(self):
        pawn = Pawn('white')
        self.chess.board[1][0] = pawn

        # Try to move the pawn to the side
        self.assertRaises(InvalidMoveException,self.chess.make_move,(1, 0), (2, 0))

        # Board state should stay the same
        self.assertEqual(pawn, self.chess.board[1][0])
        self.assertIsNone(self.chess.board[2][0])


class TestKing(unittest.TestCase):

    def setUp(self):
        self.chess = ChessGame( blank = True )
        self.chess.turn = self.chess.white
        self.king = King('white')

    def test_king_move(self):
        self.chess.board[5][5] = self.king
        self.chess.make_move((5,5),(5,6))
        self.assertEqual(self.king, self.chess.board[5][6])

    def test_king_blocked_by_other_piece_of_same_colour(self):
        another_piece = ChessPiece('white')
        self.chess.board[5][5] = self.king
        self.chess.board[5][6] = another_piece

        self.assertRaises(CannnotTakeOwnPieceException,self.chess.make_move,(5,5),(5,6))

        self.assertEqual(self.king, self.chess.board[5][5])
        self.assertEqual(another_piece, self.chess.board[5][6])

    def test_king_invalid_move(self):
        self.chess.board[1][0] = self.king

        # Try to move the king over two spaces
        self.assertRaises(InvalidMoveException,self.chess.make_move,(1, 0), (3, 0))

        # Board state should stay the same
        self.assertEqual(self.king, self.chess.board[1][0])
        self.assertIsNone(self.chess.board[2][0])


class TestBishop(unittest.TestCase):

    def setUp(self):
        self.chess = ChessGame( blank = True )
        self.chess.turn = self.chess.white
        self.bishop = Bishop('white')

    def test_bishop_move(self):
        self.chess.board[5][5] = self.bishop
        self.chess.make_move((5,5),(7,7))  # Move down and right
        self.assertEqual(self.bishop, self.chess.board[7][7])

    def test_bishop_blocked_by_other_piece(self):
        another_piece = ChessPiece('black')
        self.chess.board[5][5] = self.bishop
        self.chess.board[6][4] = another_piece

        self.assertRaises(PathObstructedException,self.chess.make_move,(5,5),(7,3))  # Try to move up and right

        self.assertEqual(self.bishop, self.chess.board[5][5])
        self.assertEqual(another_piece, self.chess.board[6][4])

    def test_bishop_invalid_move(self):
        self.chess.board[1][0] = self.bishop

        self.assertRaises(InvalidMoveException,self.chess.make_move,(1, 0), (1, 2))

        # Board state should stay the same
        self.assertEqual(self.bishop, self.chess.board[1][0])
        self.assertIsNone(self.chess.board[1][2])


class TestQueen(unittest.TestCase):

    def setUp(self):
        self.chess = ChessGame(blank=True)
        self.chess.turn = self.chess.white
        self.queen = Queen('white')

    def test_queen_move(self):
        self.chess.board[3][3] = self.queen
        self.chess.make_move((3, 3), (4, 4))
        self.assertEqual(self.queen, self.chess.board[4][4])  # Did the queen move down and right?
        self.chess.turn = self.chess.white
        self.chess.make_move((4, 4), (1, 1))
        self.assertEqual(self.queen, self.chess.board[1][1])  # Did the queen move up and left?
        self.chess.turn = self.chess.white
        self.chess.make_move((1, 1), (7, 1))
        self.assertEqual(self.queen, self.chess.board[7][1])  # Did the queen move right?
        self.chess.turn = self.chess.white
        self.chess.make_move((7, 1), (7, 7))
        self.assertEqual(self.queen, self.chess.board[7][7])  # Did the queen move down?

    def test_queen_blocked_by_other_piece(self):
        another_piece = ChessPiece('black')
        self.chess.board[5][5] = self.queen
        self.chess.board[1][5] = another_piece

        self.assertRaises(PathObstructedException, self.chess.make_move, (5, 5), (0, 5))

        self.assertEqual(self.queen, self.chess.board[5][5])
        self.assertEqual(another_piece, self.chess.board[1][5])

    def test_queen_invalid_move(self):
        self.chess.board[1][0] = self.queen

        self.assertRaises(InvalidMoveException, self.chess.make_move, (1, 0), (2, 2))

        # Board state should stay the same
        self.assertEqual(self.queen, self.chess.board[1][0])
        self.assertIsNone(self.chess.board[2][2])


class TestKnight(unittest.TestCase):

    def setUp(self):
        self.chess = ChessGame(blank=True)
        self.chess.turn = self.chess.white
        self.knight = Knight('white')

    def test_knight_move(self):
        self.chess.board[3][3] = self.knight
        self.chess.make_move((3, 3), (5, 4))
        self.assertEqual(self.knight, self.chess.board[5][4])  # Did the knight move down and right?

    def test_knight_not_blocked_by_other_piece(self):
        another_piece = ChessPiece('black')
        self.chess.board[5][5] = self.knight
        self.chess.board[4][5] = another_piece

        self.chess.make_move((5, 5), (3, 4))

        self.assertEqual(self.knight, self.chess.board[3][4])
        self.assertEqual(another_piece, self.chess.board[4][5])

    def test_knight_invalid_move(self):
        self.chess.board[1][0] = self.knight

        self.assertRaises(InvalidMoveException, self.chess.make_move, (1, 0), (1, 3))

        # Board state should stay the same
        self.assertEqual(self.knight, self.chess.board[1][0])
        self.assertIsNone(self.chess.board[2][2])

if __name__ == '__main__':
    unittest.main()



