from flask import Flask
from flask_restful import Resource, Api, reqparse
from ChessGame import ChessGame

app = Flask(__name__)
api = Api(app)
chess_game = ChessGame()

parser = reqparse.RequestParser()
parser.add_argument('start')
parser.add_argument('end')

class NewGame(Resource):
    def get(self):
        return { 'data': chess_game.board }


class MakeMove(Resource):
    def post(self):
        args = parser.parse_args()
        start = args.get('start')
        end = args.get('end')
        board.make_move(start, end)
        return {'data': chess_game.board}


api.add_resource(NewGame, '/new-game')
api.add_resource(MakeMove, '/make-move')

if __name__ == '__main__':
    app.run(port=5002)
